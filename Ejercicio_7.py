# Thomas Aranguiz -  Progra_1

#se crea la funcion para comparar las palabras
def comparar_palabras (palabras):

    """se usan los dos for para ir separando las palabras en sus letras
    para luego comparar las letras que la conforman"""
    for palabra1 in range(5):
        for palabra2 in range(palabra1+1, len(palabras)):

            #se crean las listas donde iran las letras de las palabras
            letras_palabra_1 = []
            letras_palabra_2 = []
            contador = 0

            """ los for se encarga de guardar las letras en las listas
            correspondientes y posteriormente comparar las letras
            igual a las demas de la otra palabra, para determinar
            la similitud"""
            for letra in palabras[palabra1]:
                if letra not in letras_palabra_1:
                    letras_palabra_1.append(letra)

            for letra in palabras[palabra2]:
                if letra not in letras_palabra_2:
                    letras_palabra_2.append(letra)

            for letra in letras_palabra_1:
                if letra in letras_palabra_2:
                    contador += 1

            """ se divide el contador con el mayor, para sacar el porcentaje"""
            if len(letras_palabra_1) < len(letras_palabra_2):
                mayor = len(letras_palabra_2)
            else:
                mayor = len(letras_palabra_1)

            identidad = (contador / mayor)*100

            """ declaro que mayor o igual a 70 seran semejantes
            y que menor a 5, no son semejantes"""
            if identidad > 69:
                print(f"la palabra '{palabras[palabra1]}' y '{palabras[palabra2]}'"
                      f" son las más parecidas")

            if identidad < 5:
                print(f"la palabra '{palabras[palabra1]}' y '{palabras[palabra2]}'"
                      f" son las menos parecidas")

# se piden las palabras, para luego compararlas
palabras = []
for i in range(5):
    palabras.append(input(f"Ingrese la palabra {i+1}: "))

comparar_palabras(palabras)
