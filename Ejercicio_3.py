# Thomas Aranguiz -  Progra_1

texto = input("Ingrese un texto: ")

# control de excepcion
if len(texto) < 1:
    print("No ingreso nada")
    exit(1)

palabra = input("Ingrese una palabra, que quiera compobar: ")

# control de excepcion
if len(palabra) < 1:
    print("No ingreso nada")
    exit(1)

# se usa el split para poder compararlo
texto = texto.split()

# se compara la palabra si se encuentra o no, en la oracion
if palabra in texto:
    print("Esta la palabra")
else:
    print("No esta la palabra")
