# Thomas Aranguiz -  Progra_1

# Se utiliza el split para fragmentar la lista
frase = input("Ingrese una palabra:\n").split()

# se usa este control de excepcion para cuando el usario no escribe nada
if len(frase) < 1:
    print("No ingreso nada")
    exit(1)

# El reverse para invertir el orden
frase.reverse()

# para volver a unir la lista anteriormente fragmentada
print(" ".join(frase))
