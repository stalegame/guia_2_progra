# Thomas Aranguiz -  Progra_1

# se inicializa en minusculas, para facilitar la comparacion
texto = input("Ingrese un texto: ")
texto = texto.lower()

# control de excepcion
if len(texto) < 1:
    print("No ingreso nada")
    exit(1)

# se inicializa en minusculas, para facilitar la comparacion
palabra = input("Ingrese una palabra, que quiera compobar: ")
palabra = palabra.lower()

# control de excepcion
if len(palabra) < 1:
    print("No ingreso nada")
    exit(1)

# se usa el split para poder compararlo
texto = texto.split()

# se compara la palabra si se encuentra o no, en la oracion
if palabra in texto:
    print("Esta la palabra")
else:
    print("No esta la palabra")
