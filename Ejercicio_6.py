# Thomas Aranguiz - Progra_1

print("cuando quiera terminar ingrese 'fin'\n")

# Se inicializa, para evitar errores
string_final = ""
palabra_ingresada = ""

# Se usa el while para determinar cuando se desea terminar
while palabra_ingresada != "fin":
    palabra_ingresada = input("Ingrese una palabra: ")

    # Control de excepcion
    if len(palabra_ingresada) < 1:
        print("No ingreso nada")
        exit(1)

    # Contador para aculular las palabras ingresadas
    string_final += palabra_ingresada

# Imprimir las palabrass ingresadas a excepcion del "fin"
print(string_final.replace("fin", ""))
