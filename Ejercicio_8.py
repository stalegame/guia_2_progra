# Thomas Aranguiz -  Progra_1

from os import system

palabra = input("Ingrese la palabra base: ")

# control de excepcion
if len(palabra) < 1:
    print("No ingreso nada")
    exit(1)

tiradas = []
vidas = 5

# while que controla que el juego com inicie y termine
while True:

    """ el for crea y comprueba que las letras que todavia no son introducidas
    sigan siendo guion bajo, sino la cambia por la letra en cuestion"""
    completa = True
    for letra in palabra:
        if letra not in tiradas and letra != " ":
            print("_", end="")
            completa = False
        else:
            print(letra, end="")
    print("")

    # si la palabra se completa se termina el juego
    if completa:
        break

    # Se muestras las vidas restantes y la el que se introduscan letras
    print(f"vida actual {vidas}")
    nueva = input("Ingrese una letra: ")

    if len(nueva) < 1:
        print("No ingreso nada")
        exit(1)

    # Si la letra es correcta, se almacena, sino se pierde vida
    if nueva not in tiradas:
        tiradas.append(nueva)
    if nueva not in palabra:
        vidas -= 1

        #regulador para finalizar al perder
        if vidas == 0:
            print("Perdiste")
            print(f"La palabra era {palabra}")
            break

    system("clear")
