# Thomas Aranguiz -  Progra_1

codigo_misterioso = "¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"

print(codigo_misterioso)
print("")

#Se eliminan las X
codigo_misterioso = codigo_misterioso.replace("X", "")

# se usa el [::-1] para recorrer desde el final al principio
print("El mensaje codificado es:\n")
print(codigo_misterioso[::-1])
